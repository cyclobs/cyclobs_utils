from setuptools import setup

setup(name='cyclobs_utils',
      description='cyclobs_utils',
      url='https://gitlab.ifremer.fr/cyclobs/cyclobs_utils',
      author="Theo Cevaer",
      author_email="Theo.Cevaer@ifremer.fr",
      license='GPL',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      packages=['cyclobs_utils'],
      zip_safe=False,
      #install_requires=[
      #    'pandas',
      #    'sqlalchemy',
      #    'cyclobs_orm>=0.0.dev0',
      #]
    )
