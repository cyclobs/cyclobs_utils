import collections
import logging

from sqlalchemy import create_engine, and_, distinct
from sqlalchemy.orm import sessionmaker
import numpy as np
import pandas as pd
from cyclobs_orm import Track
import datetime
from shapely import wkt
from shapely.wkt import loads
import geopandas as gpd
import math
import cmath
from pyproj import Geod
from scipy.stats import circstd
from urllib.parse import quote


logger = logging.getLogger(__name__)

map_fix_atcf_type = {
    "DVTS": "Subjective Dvorak",
    "DVTO": "Objectve Dvorak",
    "SSMI": "Special Sensor Microwave Imager",
    "SSMS": "SSM/IS (Shared Processing Program)",
    "QSCT": "QuikSCAT",
    "ERS2": "European Remote Sensing System scatterometer",
    "SEAW": "SeaWinds  scatterometer",
    "TRMM": "Tropical Rainfall Measuring Mission microwave and vis/ir imager",
    "WIND": "WindSat, polarimetric microwave radiometer",
    "MMHS": "Micorwave Humidity Sounder",
    "ALTG": "Geosat altimeter",
    "AMSU": "Advanced Microwave Sounding Unit",
    "RDRC": "Conventional radar",
    "RDRD": "Doppler radar",
    "RDRT": "TRMM radar",
    "SYNP": "Synoptic",
    "AIRC": "Aircraft",
    "ANAL": "Analysis",
    "DRPS": "Dropsonde",
    "ASCT": "Ascat",
    "RS2": "Radarsat-2",
    "SEN1": "Sentinel-1",
}

# Define a dictionary to map database types to Python types
db_python_type_map = {
    "CHAR": str,
    "VARCHAR": str,
    "TEXT": str,
    "INTEGER": int,
    "SMALLINT": int,
    "BIGINT": int,
    "BOOLEAN": bool,
    "REAL": float,
    "DOUBLE PRECISION": float,
    "NUMERIC": float,
    "DECIMAL": float,
    "DATE": "datetime.date",
    "TIMESTAMP": "datetime.datetime",
    # Add other type mappings as needed
}


def check_db_dataframe_type_compatibility(
    dataframe_type: str, db_type: str, strict=False
):
    """
    Check if the given dataframe data type and database data type are compatible

    Parameters
    ==========
    dataframe_type : str
        String representation of a DataFrame data type
    db_type : str
        String representation of a database data type

    Returns
    =======
    bool
        True if types are compatible, False otherwise
    """
    type_map = {
        "int": ["INT", "BIGINT", "SMALLINT"],
        "int64": ["INT", "BIGINT", "SMALLINT"],
        "float": ["FLOAT", "DOUBLE PRECISION"],
        "float64": ["FLOAT", "DOUBLE PRECISION"],
        "string": ["VARCHAR", "CHAR", "TEXT"],
        "object": ["VARCHAR", "CHAR", "TEXT"],
        "bool": ["BOOLEAN"],
        "datetime": ["TIMESTAMP", "DATETIME"],
        "date": ["DATE"],
        "time": ["TIME"],
        "bytes": ["BLOB"],
        "geometry": ["GEOGRAPHY"],
    }

    if not strict:
        type_map["int"] = ["INT", "BIGINT", "SMALLINT", "FLOAT", "DOUBLE PRECISION"]
        type_map["int64"] = ["INT", "BIGINT", "SMALLINT", "FLOAT", "DOUBLE PRECISION"]

    df_type = str(dataframe_type).lower()
    db_type = str(db_type).upper()

    if df_type.startswith("datetime"):
        df_type = "datetime"
    return any(
        db_type.startswith(db_type_str) for db_type_str in type_map.get(df_type, [])
    )


def coriolis(lat):
    """
    Compute coriolis force at given latitude.

    Parameters
    ----------
    lat: float latitude in degrees

    Returns
    -------
    float
        Coriolis force

    """
    Omega = 7.2921e-5  # Earth rotation vector
    f = (
        2 * Omega * np.sin(lat * np.pi / 180)
    )  # Coriolis parameter at lat° latitude and assuming it's constant
    return f


def get_rmax_ck22(Vmax, R17, fcor, intercept, coef1, coef2):
    """
    Alternate method to compute an rmax based on R34 (or R17 in m/s) and vmax

    Parameters
    ----------
    Vmax: float cyclone vmax
    R17: float radius R17 in m/s or R34 in knots
    fcor: float coriolis force affecting cyclone
    intercept: float coeff
    coef1: float coeff
    coef2: float coeff

    Returns
    -------
    float
        Computed Rmax value

    """
    M17 = R17 * 1000 * 17.5 + 0.5 * fcor * ((R17 * 1000) ** 2)
    M_ratio = intercept * np.exp(
        coef1 * (Vmax - 17.5) + coef2 * (Vmax - 17.5) * 0.5 * fcor * R17 * 1000
    )
    Mmax = M_ratio * M17
    Rmax = (Vmax / fcor) * (np.sqrt(1 + (2 * fcor * Mmax) / (Vmax**2)) - 1)
    return Rmax


def rmax_ck22(track_point: str, vmax_ms: float, rad34: float):
    """
    Compute and return rmax_ck22

    Parameters
    ----------
    track_point: str WKT representation of the TC location (Point)
    vmax_ms: float TC vmax in m/s
    rad34: float TC R34. Can typically be the mean of the four R34 per quadrant.

    Returns
    -------
    float
        RMAX ck22 value in meter
    """

    track_center = loads(track_point)

    rmax_ck22_val = round(
        get_rmax_ck22(
            vmax_ms,
            rad34,
            (
                coriolis(track_center.y)
                if track_center.y > 0
                else abs(coriolis(track_center.y))
            ),
            0.482,
            0.00309,
            -0.00304,
        )
        / 1000,
        2,
    )
    return rmax_ck22_val


def nmi_to_km(nmi_val: float):
    """
    Convert a value in nautical miles to kilometers

    Parameters
    ----------
    nmi_val: float the value to convert

    Returns
    -------
    float
        Value converted to km

    """
    return nmi_val * 1.85200


def km_to_nmi(km_val: float):
    """
    Convert a value in kilometers to convert in nautical miles

    Parameters
    ----------
    km_val: float the value to convert

    Returns
    -------
    float
        Value converted to nmi

    """
    return km_val * 0.5399568035


def knots_to_ms(knots_val: float):
    """
    Convert a value in knots to meters per second

    Parameters
    ----------
    knots_val: float the value to convert

    Returns
    -------
    float
        Value converted to m/s

    """
    return knots_val * 0.514444


def ms_to_knots(ms_val: float):
    """
    Convert a value from meters per second to knots

    Parameters
    ----------
    ms_val: float the value to convert

    Returns
    -------
    float
        Value converted to knots

    """
    return ms_val * 1.94384


# Dict to convert from string categories to their corresponding maximal wind_speed
# See also function spd2cat
# tuples are : (categ, max value for that category, in knots)
maxspd = collections.OrderedDict(
    [
        ("dep", 34),
        ("storm", 64),
        ("cat-1", 82),
        ("cat-2", 95),
        ("cat-3", 112),
        ("cat-4", 136),
        ("cat-5", 999),
    ]
)

categories = ["dep", "storm", "cat-1", "cat-2", "cat-3", "cat-4", "cat-5"]


# Converts wind speed (int) (knots or m/s) to cyclone category
def spd2cat(spd, unit="knots"):
    """
    Converts wind speed (int) in m/s into cyclone category

    Parameters
    ----------
    spd : int Wind speed in meters per second.

    Returns
    -------
    str
        The cyclone category (dep, storm, cat-X)
    """
    # Convert to knot
    if unit == "m/s":
        spd = ms_to_knots(spd)
    return list(maxspd.keys())[
        [i for i, m in enumerate(maxspd) if maxspd[m] - spd > 0][0]
    ]


def track_data(
    sid,
    around_date,
    base_url="https://cyclobs.ifremer.fr/app/",
    freq=50,
    days_before=3,
    days_after=3,
    commit=None,
    config=None,
    listing=None,
):
    """
    Query CyclObs track API to retrieve track data 3 days around a given datetime.

    Parameters
    ----------
    sid: str containing storm sid to request
    around_date: datetime around which request the track points (3 days around)
    base_url: str Host URL to request from
    freq: int Transferred to API request argument freq, which is the wanted interval in minutes between track points
    days_before: int amount of days before around_date to request
    days_after: int amount of days after around_date to request

    Returns
    -------
    pandas.DataFrame
        DataFrame containing result of cyclobs track API
    """
    min_date = (around_date - datetime.timedelta(days=days_before)).strftime("%Y-%m-%d")
    max_date = (around_date + datetime.timedelta(days=days_after)).strftime("%Y-%m-%d")
    api_req = f"{base_url}api/track?include_cols=all&sid={quote(sid)}&freq={quote(freq)}&min_date={quote(min_date)}&max_date={quote(max_date)}"
    if commit is not None:
        api_req += f"&commit={quote(commit)}"
    if config is not None:
        api_req += f"&config={quote(config)}"
    if listing is not None:
        api_req += f"&listing={quote(listing)}"
    logger.info(f"Getting data from Cyclobs track API. Req : {str(api_req)}")

    track = pd.read_csv(api_req)

    track["geometry"] = track["geometry"].apply(wkt.loads)
    track = gpd.GeoDataFrame(track, geometry="geometry")

    return track


def get_all_missions(host, short=False, commit=None, config=None, listing=None):
    if not short:
        col = "mission"
        api_req = host + "/app/api/getData?include_cols=mission"
    else:
        col = "mission_short"
        api_req = host + "/app/api/getData?include_cols=mission_short"
    if commit is not None:
        api_req += f"&commit={quote(commit)}"
    if config is not None:
        api_req += f"&config={quote(config)}"
    if listing is not None:
        api_req += f"&listing={quote(listing)}"
    df = pd.read_csv(api_req)
    return df[col].tolist()


def get_all_instruments(host, short=False, commit=None, config=None, listing=None):
    if not short:
        col = "instrument"
        api_req = host + "/app/api/getData?include_cols=instrument"
    else:
        col = "instrument_short"
        api_req = host + "/app/api/getData?include_cols=instrument_short"
    if commit is not None:
        api_req += f"&commit={quote(commit)}"
    if config is not None:
        api_req += f"&config={quote(config)}"
    if listing is not None:
        api_req += f"&listing={quote(listing)}"
    df = pd.read_csv(api_req)
    return df[col].tolist()


def get_missions_per_instrument(
    host, short=False, commit=None, config=None, listing=None
):
    if not short:
        col = "instrument"
        col_m = "mission"
        api_req = host + "/app/api/getData?include_cols=mission,instrument"
    else:
        col = "instrument_short"
        col_m = "mission_short"
        api_req = host + "/app/api/getData?include_cols=mission_short,instrument_short"
    if commit is not None:
        api_req += f"&commit={quote(commit)}"
    if config is not None:
        api_req += f"&config={quote(config)}"
    if listing is not None:
        api_req += f"&listing={quote(listing)}"
    df = pd.read_csv(api_req)

    df = df.groupby([col])[col_m].apply(list)

    return df.to_dict()


# Returns the following structure
# { instrumentName : [mission1, mission2, ... ] }


def track_sources(dbd):
    """
    Retrieves the list of available track source.
    The function reads the Track table, and finds all the distinct track sources.

    Returns
    -------
    list
        The list of track sources (as strings)
    """

    engine = create_engine(dbd)
    Session = sessionmaker(bind=engine)
    sess = Session()

    req = sess.query(distinct(Track.source)).all()

    return [r[0] for r in req]


def filter_df(
    df, list_instruments=None, list_missions=None, basin=None, year=None, short=False
):
    if basin is not None:
        df = df.loc[df["basin"] == basin]

    if year is not None:
        start = datetime.datetime(year, 1, 1)
        stop = datetime.datetime(year, 12, 31)
        df = df.loc[
            (df["acquisition_start_time"] >= start)
            & (df["acquisition_start_time"] <= stop)
        ]

    if list_instruments is not None:
        if not short:
            df = df.loc[df["instrument"].isin(list_instruments)]
        else:
            df = df.loc[df["instrument_short"].isin(list_instruments)]

    if list_missions is not None:
        if not short:
            df = df.loc[df["mission"].isin(list_missions)]
        else:
            df = df.loc[df["mission_short"].isin(list_missions)]

    return df


class DataFrameDs:
    """
    Helper class to store a xarray Dataset inside a pandas DataFrame.
    """

    def __init__(self, df):
        self.df = df
        # self.ds is a dict {df_index: ds}
        self.ds = {}

    def set_ds(self, ds):
        try:
            k = list(ds.keys())
            self.df.loc[k]
        except KeyError:
            msg = "The given dict must have the dataframe index (or a subset of the index) as keys"
            logger.error(msg)
            raise ValueError(msg)
        self.ds = ds

    def loc_ds(self, bool_list):
        sub_df_index = self.df.loc[bool_list].index
        ds_list = [self.ds[idx] for idx in sub_df_index.tolist()]

        return ds_list


def dir_from_dates(track_df, date1, date2):
    track_df["diff1"] = track_df["date"].apply(
        lambda x: abs((x - date1).total_seconds())
    )
    min_d1 = track_df["diff1"].idxmin()
    lon1 = track_df.iloc[min_d1]["lon"]
    lat1 = track_df.iloc[min_d1]["lat"]
    date_track_1 = track_df.iloc[min_d1]["date"]
    logger.debug(f"Date1: {date1}, date_track_1: {date_track_1}")
    if abs(date_track_1 - date1) > datetime.timedelta(minutes=7, seconds=30):
        logger.warning(f"No track data for date {date1}. Skipping.")
        return None, None

    track_df["diff2"] = track_df["date"].apply(
        lambda x: abs((x - date2).total_seconds())
    )
    min_d2 = track_df["diff2"].idxmin()
    lon2 = track_df.iloc[min_d2]["lon"]
    lat2 = track_df.iloc[min_d2]["lat"]
    date_track_2 = track_df.iloc[min_d2]["date"]
    logger.debug(f"Date2: {date2}, date_track_2: {date_track_2}")
    if abs(date_track_2 - date2) > datetime.timedelta(minutes=7, seconds=30):
        logger.warning(f"No track data for date {date2}. Skipping.")
        return None, None

    geod = Geod(ellps="WGS84")
    faz, baz, dist = geod.inv(lon1, lat1, lon2, lat2)

    time_diff = (date_track_2 - date_track_1).total_seconds()
    # dist in meters, time_diff in seconds, so speed in meters per second
    speed = dist / time_diff

    return -faz, speed


def cyclone_speed_dir_angle_to_north(
    sid: str,
    acq_datetime: datetime.datetime,
    base_url="https://cyclobs.ifremer.fr/app/",
    interval_minutes=15,
    tracks_df=None,
    commit=None,
    config=None,
    listing=None,
):
    """
    Compute cyclone speed and direction using interpolated track points.
    Several track point are used to compute a more accurate direction and speed.

    The function compute speed and direction for several intervals :
    - acq_date - 3 hours -> acq_datetime
    - acq_date - 6 hours -> acq_datetime
    - acq_date - 3 hours -> acq_datetime + 3 hours
    - acq_date -> acq_datetime + 3 hours
    - acq_date -> acq_datetime + 6 hours
    These computed direction and speed are stored as complex numbers.

    Then, the final speed is computed as the mean of the absolute values of all the stored complex numbers.
    The final direction is computed by averaging the complex numbers and taking out the argument of that mean.

    Parameters
    ----------
    sid: str storm id
    acq_datetime: datetime.datetime acquisition datetime
    base_url: str url to the API to request
    interval_minutes: int interval in minutes to query the API
    tracks_df: pandas.DataFrame Dataframe containing track data for that cyclone. If given, base_url and
    interval_minutes can be omitted (and will not be used).

    Returns
    -------
    float, float, float, float
    Cyclone direction, cyclone direction standard deviation, cyclone speed, cyclone speed standard deviation
    """

    if tracks_df is None:
        min_date = (acq_datetime - datetime.timedelta(days=2)).strftime("%Y-%m-%d")
        max_date = (acq_datetime + datetime.timedelta(days=2)).strftime("%Y-%m-%d")
        api_req = f"{base_url}/api/track?include_cols=all&sid={quote(sid)}&freq={quote(str(interval_minutes))}&min_date={quote(min_date)}&max_date={quote(max_date)}"
        if commit is not None:
            api_req += f"&commit={quote(commit)}"
        if config is not None:
            api_req += f"&config={quote(config)}"
        if listing is not None:
            api_req += f"&listing={quote(listing)}"
        logger.info(f"Getting data from Cyclobs track API. Req : {str(api_req)}")
        logger.info(f"Acquisition datetime is {acq_datetime}")
        track = pd.read_csv(api_req)
    else:
        track = tracks_df
    track["date"] = pd.to_datetime(track["date"])

    dates_track = [
        (acq_datetime - datetime.timedelta(hours=3), acq_datetime),
        (acq_datetime - datetime.timedelta(hours=6), acq_datetime),
        (
            acq_datetime - datetime.timedelta(hours=3),
            acq_datetime + datetime.timedelta(hours=3),
        ),
        (acq_datetime, acq_datetime + datetime.timedelta(hours=3)),
        (acq_datetime, acq_datetime + datetime.timedelta(hours=6)),
    ]

    dirs = []
    dir_spd_complex = []

    for d in dates_track:
        direction, spd = dir_from_dates(track, d[0], d[1])
        logger.debug(f"Direction for dates {d}, {direction}, {spd}")
        if direction is not None:
            dirs.append(math.radians(direction))
            dir_spd_complex.append(cmath.rect(spd, math.radians(direction)))

    if len(dir_spd_complex) == 0:
        logger.error(
            "No track data could be found to compute cyclone direction and speed."
        )

    if len(dir_spd_complex) == 1:
        logger.warning(
            "Direction and speed will be computed using only 2 track points because no "
            "other could be found"
        )

    logger.debug(f"Dir speed complex {dir_spd_complex}")
    dir_spd_mean_complex = np.mean(dir_spd_complex)
    dir_mean = math.degrees(cmath.phase(dir_spd_mean_complex))
    dir_std = circstd(dirs)
    speed_mean = np.mean(np.abs(dir_spd_complex))
    logger.debug(f"dir speed mean {speed_mean}")
    speed_std = np.std(dir_spd_complex)

    logger.info(
        f"Found propagation angle to north is {dir_mean}°, dir std {dir_std}, speed : {speed_mean} m/s, "
        f"speed_std {speed_std}"
    )

    return dir_mean, dir_std, speed_mean, speed_std
